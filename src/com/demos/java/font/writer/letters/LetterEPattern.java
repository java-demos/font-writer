package com.demos.java.font.writer.letters;

import com.demos.java.font.writer.enums.MatrixPatterns;
import com.demos.java.font.writer.matrices.MatrixPatternFactory;
import com.demos.java.font.writer.utils.MatrixOperation;
import com.demos.java.font.writer.utils.MatrixOverlap;

public class LetterEPattern implements LetterPattern {

	private final int matrixHeight;
	private final int matrixWidth;

	private String[][] matrix;

	public LetterEPattern(final int matrixHeight, final int matrixWidth) {
		super();
		this.matrixHeight = matrixHeight;
		this.matrixWidth = matrixWidth;
		this.matrix = new String[this.matrixHeight][this.matrixWidth];
	}

	@Override
	public String[][] generateMatrix() {
		final MatrixPatternFactory matrixFactory = new MatrixPatternFactory(this.matrixHeight,
				this.matrixWidth);

		final MatrixOperation matrixOverlap = new MatrixOverlap();

		this.matrix = matrixOverlap.compute(this.matrix,
				matrixFactory.getPattern(MatrixPatterns.TOP_ROW));
		this.matrix = matrixOverlap.compute(this.matrix,
				matrixFactory.getPattern(MatrixPatterns.MIDDLE_ROW));
		this.matrix = matrixOverlap.compute(this.matrix,
				matrixFactory.getPattern(MatrixPatterns.BOTTOM_ROW));
		this.matrix = matrixOverlap.compute(this.matrix,
				matrixFactory.getPattern(MatrixPatterns.LEFT_COLUMN));

		return this.matrix;
	}

}
