package com.demos.java.font.writer.letters;

import com.demos.java.font.writer.enums.MatrixPatterns;
import com.demos.java.font.writer.matrices.MatrixPatternFactory;
import com.demos.java.font.writer.utils.MatrixHorizontalAdd;
import com.demos.java.font.writer.utils.MatrixOperation;
import com.demos.java.font.writer.utils.MatrixOverlap;

public class LetterAPattern implements LetterPattern {

	private final int matrixHeight;
	private final int matrixWidth;

	private String[][] matrix;

	public LetterAPattern(final int matrixHeight, final int matrixWidth) {
		super();
		this.matrixHeight = matrixHeight;
		this.matrixWidth = matrixWidth;
		this.matrix = new String[this.matrixHeight][this.matrixWidth];
	}

	@Override
	public String[][] generateMatrix() {
		MatrixPatternFactory matrixFactory = new MatrixPatternFactory(this.matrixHeight,
				this.matrixWidth);

		final MatrixOperation matrixOverlap = new MatrixOverlap();
		final MatrixOperation matrixHorizontalAdd = new MatrixHorizontalAdd();

		this.matrix = matrixHorizontalAdd.compute(
				matrixFactory.getPattern(MatrixPatterns.FORWARD_SLASH),
				matrixFactory.getPattern(MatrixPatterns.BACKWARD_SLASH));

		matrixFactory = new MatrixPatternFactory(this.matrixHeight, (this.matrixWidth * 2) + 1);

		this.matrix = matrixOverlap.compute(this.matrix,
				matrixFactory.getPattern(MatrixPatterns.MIDDLE_ROW));

		return this.matrix;
	}

}
