package com.demos.java.font.writer.letters;

import com.demos.java.font.writer.matrices.EmptyMatrixPattern;

public class LetterMatrixFactory {

	private final int matrixHeight;
	private final int matrixWidth;

	public LetterMatrixFactory(final int matrixHeight, final int matrixWidth) {
		super();
		this.matrixHeight = matrixHeight;
		this.matrixWidth = matrixWidth;
	}

	public String[][] getLetterMatrix(final String letter) {
		final String uLetter = letter.toUpperCase();
		switch (uLetter) {
		case "E":
			return new LetterEPattern(this.matrixHeight, this.matrixWidth).generateMatrix();
		case "F":
			return new LetterFPattern(this.matrixHeight, this.matrixWidth).generateMatrix();
		case "I":
			return new LetterIPattern(this.matrixHeight, this.matrixWidth).generateMatrix();
		case "A":
			return new LetterAPattern(this.matrixHeight, this.matrixWidth).generateMatrix();
		default:
			break;
		}
		return new EmptyMatrixPattern(this.matrixHeight, this.matrixWidth).generateMatrix();
	}

}
