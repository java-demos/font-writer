package com.demos.java.font.writer.letters;

public interface LetterPattern {

	String[][] generateMatrix();

}
