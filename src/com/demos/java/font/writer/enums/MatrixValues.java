package com.demos.java.font.writer.enums;

public enum MatrixValues {

	ASTERIX("*"), BLANK(" ");

	private String value;

	private MatrixValues(final String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

}
