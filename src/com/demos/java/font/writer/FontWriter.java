package com.demos.java.font.writer;

import java.util.ArrayList;
import java.util.List;

import com.demos.java.font.writer.enums.MatrixPatterns;
import com.demos.java.font.writer.letters.LetterMatrixFactory;
import com.demos.java.font.writer.matrices.MatrixPatternFactory;
import com.demos.java.font.writer.utils.MatrixHorizontalAdd;
import com.demos.java.font.writer.utils.MatrixOperation;
import com.demos.java.font.writer.utils.MatrixVerticalAdd;

public class FontWriter {

	public static void main(final String[] args) {
		final int matrixSize = 10;
		final String letters = "aef";
		final LetterMatrixFactory letterMatrixFactory = new LetterMatrixFactory(matrixSize,
				matrixSize);

		final List<String[][]> lettersMatrices = new ArrayList<>();

		for (int i = 0; i < letters.length(); i++) {
			lettersMatrices
					.add(letterMatrixFactory.getLetterMatrix(String.valueOf(letters.charAt(i))));
		}

		final MatrixOperation matrixHorizontalAdd = new MatrixHorizontalAdd();
		final MatrixOperation matrixVerticalAdd = new MatrixVerticalAdd();
		final MatrixPatternFactory matrixFactory = new MatrixPatternFactory(matrixSize, matrixSize);

		String[][] horizontalDisplay = matrixHorizontalAdd.compute(lettersMatrices.get(0),
				matrixFactory.getPattern(MatrixPatterns.EMPTY_COLUMN));
		String[][] verticalDisplay = matrixVerticalAdd.compute(lettersMatrices.get(0),
				matrixFactory.getPattern(MatrixPatterns.EMPTY_ROW));

		for (int i = 1; i < lettersMatrices.size(); i++) {
			horizontalDisplay = matrixHorizontalAdd.compute(horizontalDisplay,
					lettersMatrices.get(i));
			horizontalDisplay = matrixHorizontalAdd.compute(horizontalDisplay,
					matrixFactory.getPattern(MatrixPatterns.EMPTY_COLUMN));
			verticalDisplay = matrixVerticalAdd.compute(verticalDisplay, lettersMatrices.get(i));
			verticalDisplay = matrixVerticalAdd.compute(verticalDisplay,
					matrixFactory.getPattern(MatrixPatterns.EMPTY_ROW));

		}

		FontWriter.printMatrix(horizontalDisplay);
		FontWriter.printMatrix(verticalDisplay);
	}

	private static void printMatrix(final String[][] matrix) {
		for (final String[] row : matrix) {
			for (final String column : row) {
				System.out.print(column);
			}
			System.out.println("");
		}
	}

}
