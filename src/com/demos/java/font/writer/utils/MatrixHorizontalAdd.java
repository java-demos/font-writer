package com.demos.java.font.writer.utils;

import com.demos.java.font.writer.enums.MatrixValues;

public class MatrixHorizontalAdd implements MatrixOperation {

	@Override
	public String[][] compute(final String[][] firstOperand, final String[][] secondOperand) {

		int firstOperandMaxWitdh = firstOperand[0].length;
		for (int i = 1; i < firstOperand.length; i++) {
			firstOperandMaxWitdh = firstOperand[i].length > firstOperandMaxWitdh
					? firstOperand[i].length
					: firstOperandMaxWitdh;
		}

		int secondOperandMaxWitdh = secondOperand[0].length;
		for (int i = 1; i < secondOperand.length; i++) {
			secondOperandMaxWitdh = secondOperand[i].length > secondOperandMaxWitdh
					? secondOperand[i].length
					: secondOperandMaxWitdh;
		}

		final int maxHeight = Math.max(firstOperand.length, secondOperand.length);

		final int totalWidth = firstOperandMaxWitdh + secondOperandMaxWitdh;

		final String[][] result = new String[maxHeight][totalWidth];

		for (int i = 0; i < maxHeight; i++) {
			if ((i < firstOperand.length) && (i < secondOperand.length)) {
				for (int j = 0; j < totalWidth; j++) {
					if (j < firstOperand[i].length) {
						result[i][j] = firstOperand[i][j];
					} else if ((j - firstOperand[i].length) < secondOperand[i].length) {
						result[i][j] = secondOperand[i][j - firstOperand[i].length];
					}
				}
			} else if (i < firstOperand.length) {
				for (int j = 0; j < totalWidth; j++) {
					if (j < firstOperand[i].length) {
						result[i][j] = firstOperand[i][j];
					} else {
						result[i][j] = MatrixValues.BLANK.getValue();
					}
				}
			} else if (i < secondOperand.length) {
				for (int j = 0; j < totalWidth; j++) {
					if (j < secondOperand[i].length) {
						result[i][j] = secondOperand[i][j];
					} else {
						result[i][j] = MatrixValues.BLANK.getValue();
					}
				}
			}
		}

		return result;
	}

}
