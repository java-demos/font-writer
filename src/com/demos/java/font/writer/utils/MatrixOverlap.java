package com.demos.java.font.writer.utils;

import com.demos.java.font.writer.enums.MatrixValues;

public class MatrixOverlap implements MatrixOperation {

	@Override
	public String[][] compute(final String[][] firstOperand, final String[][] secondOperand) {

		final int maxHeight = Math.max(firstOperand.length, secondOperand.length);
		final String[][] result = new String[maxHeight][];

		for (int i = 0; i < maxHeight; i++) {
			if ((i < firstOperand.length) && (i < secondOperand.length)) {

				final int maxWidth = Math.max(firstOperand[i].length, secondOperand[i].length);
				result[i] = new String[maxWidth];

				for (int j = 0; j < maxWidth; j++) {
					if ((j < firstOperand[i].length) && (j < secondOperand[i].length)) {
						if (MatrixValues.ASTERIX.getValue().equals(firstOperand[i][j])) {
							result[i][j] = firstOperand[i][j];
						} else if (MatrixValues.ASTERIX.getValue().equals(secondOperand[i][j])) {
							result[i][j] = secondOperand[i][j];
						} else {
							result[i][j] = MatrixValues.BLANK.getValue();
						}

					} else if (j < firstOperand[i].length) {
						result[i][j] = firstOperand[i][j];
					} else if (j < secondOperand[i].length) {
						result[i][j] = secondOperand[i][j];
					}
				}

			} else if (i < firstOperand.length) {
				result[i] = firstOperand[i];
			} else if (i < secondOperand.length) {
				result[i] = secondOperand[i];
			}
		}

		return result;
	}

}
