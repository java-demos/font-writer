package com.demos.java.font.writer.utils;

public class MatrixVerticalAdd implements MatrixOperation {

	@Override
	public String[][] compute(final String[][] firstOperand, final String[][] secondOperand) {
		final int totalHeight = firstOperand.length + secondOperand.length;
		final String[][] result = new String[totalHeight][];

		for (int i = 0; i < firstOperand.length; i++) {
			result[i] = firstOperand[i];
		}
		for (int j = 0; j < secondOperand.length; j++) {
			result[j + firstOperand.length] = secondOperand[j];
		}

		return result;
	}

}
