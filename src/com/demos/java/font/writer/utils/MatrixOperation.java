package com.demos.java.font.writer.utils;

public interface MatrixOperation {

	String[][] compute(String[][] firstOperand, String[][] secondOperand);
}
