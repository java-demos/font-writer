package com.demos.java.font.writer.matrices;

import com.demos.java.font.writer.enums.MatrixValues;

public class BottomRowMatrixPattern implements MatrixPattern {

	private final int matrixHeight;
	private final int matrixWidth;

	public BottomRowMatrixPattern(final int matrixHeight, final int matrixWidth) {
		super();
		this.matrixHeight = matrixHeight;
		this.matrixWidth = matrixWidth;
	}

	@Override
	public String[][] generateMatrix() {
		final String[][] matrixPattern = new String[this.matrixHeight][this.matrixWidth];
		for (int i = 0; i < this.matrixHeight; i++) {
			for (int j = 0; j < this.matrixWidth; j++) {
				if (i == (this.matrixHeight - 1)) {
					matrixPattern[i][j] = MatrixValues.ASTERIX.getValue();
				} else {
					matrixPattern[i][j] = MatrixValues.BLANK.getValue();
				}
			}
		}
		return matrixPattern;
	}

}
