package com.demos.java.font.writer.matrices;

public interface MatrixPattern {

	String[][] generateMatrix();
}
