package com.demos.java.font.writer.matrices;

import com.demos.java.font.writer.enums.MatrixPatterns;

public class MatrixPatternFactory {

	private final int matrixHeight;
	private final int matrixWidth;

	public MatrixPatternFactory(final int matrixHeight, final int matrixWidth) {
		super();
		this.matrixHeight = matrixHeight;
		this.matrixWidth = matrixWidth;
	}

	public String[][] getPattern(final MatrixPatterns matrixPattern) {
		switch (matrixPattern) {
		case TOP_ROW:
			return new TopRowMatrixPattern(this.matrixHeight, this.matrixWidth).generateMatrix();
		case BOTTOM_ROW:
			return new BottomRowMatrixPattern(this.matrixHeight, this.matrixWidth).generateMatrix();
		case MIDDLE_ROW:
			return new MiddleRowMatrixPattern(this.matrixHeight, this.matrixWidth).generateMatrix();
		case LEFT_COLUMN:
			return new LeftColumnMatrixPattern(this.matrixHeight, this.matrixWidth)
					.generateMatrix();
		case FORWARD_SLASH:
			return new ForwardSlashMatrixPattern(this.matrixHeight, this.matrixWidth)
					.generateMatrix();
		case BACKWARD_SLASH:
			return new BackSlashMatrixPattern(this.matrixHeight, this.matrixWidth).generateMatrix();
		case EMPTY_COLUMN:
			return new EmptyColumnMatrixPattern(this.matrixHeight).generateMatrix();
		case EMPTY_ROW:
			return new EmptyRowMatrixPattern(this.matrixWidth).generateMatrix();
		default:
			break;
		}
		return new EmptyMatrixPattern(this.matrixHeight, this.matrixWidth).generateMatrix();
	}
}
